package main

import "fmt"

func main() {

	// var a declaration
	// int32 data type
	var a int32
	a = 15
	fmt.Println("Value of a  ", a)
}
