package main

import "fmt"

/*
 - Slices are more flexible than arrays, but are always reference them
 - Elements in slice and arrays are indexed start [0]
*/

var g_groceries []string

func add_grocery(a string) {
	fmt.Println("Capasity", cap(g_groceries))
	g_groceries = append(g_groceries, a)
}

func list_groceries() {
	fmt.Println("Groceries list is as follows:")
	for i := 0; i < len(g_groceries); i++ {
		fmt.Println(g_groceries[i])
	}
}

func main() {
	add_grocery("milk")
	add_grocery("Cucumebers")
	add_grocery("Vaseline")
	add_grocery("Coffee")
	add_grocery("Fruit Cake")
	add_grocery("Bread")
	add_grocery("Bananas")
	list_groceries()
}
