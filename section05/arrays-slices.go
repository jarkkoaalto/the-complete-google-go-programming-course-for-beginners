package main

import "fmt"

const g_cap int = 5 // total capasity of our list
var g_groceries [g_cap]string
var g_len int = 0 // total number of grocery items on our current list

func add_grocery(a string) {
	if g_len < g_cap {
		g_groceries[g_len] = a
		g_len++
	} else {
		fmt.Println("Too many items, now we are done for !!!")
	}
}

func list_groceries() {
	fmt.Println("Grocery list is as follows:")
	for i := 0; i < g_len; i++ {
		fmt.Println(g_groceries[i])
	}
}

func main() {
	add_grocery("Apples")
	add_grocery("Coffe")
	add_grocery("Banana")
	add_grocery("Meat")
	add_grocery("Shampoo")
	list_groceries()
}
