package main

import "fmt"

/*
Fundamentak points

- input parameters must match and are pushed on the stack!
- when a function is called, it's return is passed back to the
	expression where it was called from.int
- it's possible to use * de-reference operator and the & reference
	operator, to pass a memory location for a variable into a function.
	It's the possible to chahe the contents of that variable, while within
	a function, which can be useful for larger application.
*/

func maximi(i int, j int, k *int) {
	if i > j {
		*k = i
	} else {
		*k = j
	}
}

func main() {
	var c int
	fmt.Println(&c)
	maximi(100, 15, &c)
	fmt.Println(c)
}
