package main

import (
	"fmt"
)

/*
 Things to know about functions
 1. Arguments are evaluated expressions. The result type must match the parameters
*/

func printWow(times int) {
	for i := 0; i < times; i++ {
		fmt.Println("WOW")
	}
}

/*
2. be aware of scope
HUOM!! printme  not work
*/

func printMany(times int) {
	var i int
	i = 0
	for i < times {
		//fmt.Println(printme)
		fmt.Println("I'm awesome !")
		i++
	}
}

func reset_value(i int) {
	fmt.Println("In reset_value -i is:", i, "Located at :", &i)
	i = 0
	fmt.Println("In reset_value - i is:", i, "Located at:", &i)
}
func main() {
	printWow(10)
	//printme := "I'm awesome !"
	printMany(10)

	j := 15
	fmt.Println("In main -J is: ", j, " Located at:", &j)
	reset_value(j)
	fmt.Println("In main - J is: ", j, "Located at: ", &j)
}
