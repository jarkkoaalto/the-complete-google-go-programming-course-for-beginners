package main

import "fmt"

func main() {
	age := 421

	if age < 13 {
		fmt.Println("Wow your are young")
	} else if age < 20 {
		fmt.Println("You are teenager")
	} else if age < 30 {
		fmt.Println("You are twenties")
	} else if age < 40 {
		fmt.Println("You are thirties")
	} else if age < 50 {
		fmt.Println("You're gettting there!")
	} else {
		fmt.Println("Over the hill")
	}
}
