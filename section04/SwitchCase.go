package main

import "fmt"

func main() {
	for day := 1; day <= 12; day++ {
		fmt.Println("On the ", day, " of Crismass my true love sent to me:")

		switch day {
		case 12:
			fmt.Println("Twelve Drummers Drumming")
		case 11:
			fmt.Println("Eleven Pipes Piping")
		case 10:
			fmt.Println("The Lords a Leaping")
		case 9:
			fmt.Println("Nine Ladies Dancing")
		case 8:
			fmt.Println("Eight Maids a Milking")
		case 7:
			fmt.Println("Seven Swan a Swimming")
		case 6:
			fmt.Println("Six geese a laying")
		case 5:
			fmt.Println("Give  Gold rings")
		case 4:
			fmt.Println("Fuor Calling Birds")
		case 3:
			fmt.Println("Three French Hens")
		case 2:
			fmt.Println("Two turtle Doves")
		case 1:
			fmt.Println("a Partridher in pearl tree")
		default:
			fmt.Println("Invalid value")
		}

	}
}
