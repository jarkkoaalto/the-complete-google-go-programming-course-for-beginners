package main

import "fmt"

func main() {
	age := 12

	if age < 13 {
		fmt.Println("Wow your are young")
	}
	if age > 12 && age < 20 {
		fmt.Println("You are teenager")
	}
	if age > 19 && age < 30 {
		fmt.Println("You are twenties")
	}
	if age > 29 && age < 40 {
		fmt.Println("You are thirties")
	}
	if age > 39 && age < 50 {
		fmt.Println("You're gettting there!")
	}
	if age > 49 {
		fmt.Println("Over the hill")
	}
}
