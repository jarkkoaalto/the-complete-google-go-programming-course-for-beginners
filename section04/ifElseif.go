package main

import "fmt"

func main() {
	for day := 1; day <= 12; day++ {
		fmt.Println("On the ", day, " of Crismass my true love sent to me:")

		if day == 12 {
			fmt.Println("Twelve Drummers Drumming")
		} else if day == 11 {
			fmt.Println("Eleven Pipes Piping")
		} else if day == 10 {
			fmt.Println("The Lords a Leaping")
		} else if day == 9 {
			fmt.Println("Nine Ladies Dancing")
		} else if day == 8 {
			fmt.Println("Eight Maids a Milking")
		} else if day == 7 {
			fmt.Println("Seven Swan a Swimming")
		} else if day == 6 {
			fmt.Println("Six geese a laying")
		} else if day == 5 {
			fmt.Println("Give  Gold rings")
		} else if day == 4 {
			fmt.Println("Fuor Calling Birds")
		} else if day == 3 {
			fmt.Println("Three French Hens")
		} else if day == 2 {
			fmt.Println("Two turtle DOves")
		} else if day == 1 {
			fmt.Println("a Partridher in pearl tree")
		} else {
			fmt.Print("Invalid")
		}

	}
}
