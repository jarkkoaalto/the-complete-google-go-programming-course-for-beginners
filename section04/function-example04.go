package main

import (
	"fmt"
)

func maxi(i int, j int) int {
	if i > j {
		return i
	} else {
		return j
	}
}

func main() {
	fmt.Println(maxi(100, 15))

}
