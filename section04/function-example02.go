package main

/*
Key points:
Golang passes data into functions "by value" rather than "by reference"
1. it uses "the stack" - which is just memory goland has allocated.
2. When calling a function, it creates a copy of all the arguments and places (pushed) them on to the stack
3. When it makes this copy - it copies the "value" of those arguments , onto the
		stack "not the memory location" of them
4. because of this - it-s a "one way trip". Values don't get copied
back from the stack to the caller of the function.
*/

func reset(i int) {
	i = 0
}

func main() {
	reset(10)
}
