package main

import "fmt"

func main() {
	for day := 1; day <= 12; day++ {
		fmt.Println("On the ", day, " of Crismass my true love sent to me:")

		switch day {
		case 12:
			fmt.Println("Twelve Drummers Drumming")
			fallthrough
		case 11:
			fmt.Println("Eleven Pipes Piping")
			fallthrough
		case 10:
			fmt.Println("The Lords a Leaping")
			fallthrough
		case 9:
			fmt.Println("Nine Ladies Dancing")
			fallthrough
		case 8:
			fmt.Println("Eight Maids a Milking")
			fallthrough
		case 7:
			fmt.Println("Seven Swan a Swimming")
			fallthrough
		case 6:
			fmt.Println("Six geese a laying")
			fallthrough
		case 5:
			fmt.Println("Give  Gold rings")
			fallthrough
		case 4:
			fmt.Println("Fuor Calling Birds")
			fallthrough
		case 3:
			fmt.Println("Three French Hens")
			fallthrough
		case 2:
			fmt.Println("Two turtle Doves")
			fallthrough
		case 1:
			fmt.Println("a Partridher in pearl tree")

		default:
			fmt.Println("Invalid value")

		}

	}
}
